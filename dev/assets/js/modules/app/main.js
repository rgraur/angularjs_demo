'use strict';

angular.module(
  'app',
  [
    'app.directive.message',
    'app.controller.login',
    'app.controller.home',
  ]
)

/**
 * "App" module.
 *
 * Will define routes.
 *
 * @param  {Object} $routeProvider AngularJS router
 */
.config(['$routeProvider', function($routeProvider){
  // create routes
  $routeProvider
  .when('/login', {templateUrl: 'templates/angular/app/login.html', controller: 'LoginController'})
  .when('/home', {templateUrl: 'templates/angular/app/home.html', controller: 'HomeController'})
  .otherwise({redirectTo: 'login/'});
}]);