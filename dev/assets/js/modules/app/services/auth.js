'use strict';

angular.module('app.service.auth', [])

/**
 * Service to login/logout user.
 *
 * @param  {Object} $location location dependency
 */
.factory('AuthService', ['$location', function($location){
  return {
    /**
     * Check credentials and redirect home.
     */
    login: function(credentials){
      if(credentials.username === 'user' && credentials.password === 'pass'){
        $location.path('/home');
      }
    },

    /**
     * Redirect to login.
     */
    logout: function(){
      $location.path('/login');
    }
  };
}]);