'use strict';

angular.module('app.directive.message', [])

/**
 * Mouse over/out details directive.
 *
 * Changes details text.
 *
 * @return {Object} directive
 */
.directive('appShowMessageWhenHovered', function(){
  return{
    restrict: 'A', // A = attribute
    link: function(scope, element, attributes){
      var oldDetails = scope.details;

      // define handlers
      element
      .bind('mouseover', function(){
        scope.details = attributes.details;
        scope.$apply();
      })
      .bind('mouseout', function(){
        scope.details = oldDetails;
        scope.$apply();
      });
    }
  };
});