'use strict';

angular.module(
  'app.controller.home',
  [
    'app.service.auth'
  ]
)

/**
 * Home controller.
 *
 * Populates page variables.
 *
 * @param  {Object} $scope application scope
 */
.controller('HomeController', ['$scope', 'AuthService', function($scope, AuthService){
  $scope.title   = 'user';
  $scope.details = 'Hover me!';

  // logout user
  $scope.logout = function(){ AuthService.logout(); };
}]);