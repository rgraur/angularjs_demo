'use strict';

angular.module(
  'app.controller.login',
  [
    'app.service.auth'
  ]
)

/**
 * Login controller.
 *
 * Checks credentials and redirects to homepage.
 *
 * @param  {Object} $scope application scope
 */
.controller('LoginController', ['$scope', 'AuthService', function($scope, AuthService){
  $scope.credentials = {username: '', password: ''};

  // login user
  $scope.login = function(){ AuthService.login($scope.credentials); };
}]);