module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    // create prod structure
    copy: {
      // all js (excluding vendor folder), all css, all html (excluding index)
      all_html_css_js: {
        files: [{
          expand: true,
          cwd: 'dev/',
          src: ['**', '!assets/**', '!index.*.html'],
          dest: 'prod/'
        }]
      },
      // index for prod
      index: {
        files: [{
          src: ['dev/index.prod.html'],
          dest: 'prod/index.html'
        }]
      }
    },

    // concatenate js task
    concat: {
      options: {
        // remove all "use strict" statements (will be added a single one in "uglify" task banner)
        process: function(src, filepath){
          return '// Source: ' + filepath + '\n' + src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');
        },
      },
      prod_js: {
        src: [
          'dev/assets/js/vendor/angular/angular.js',
          'dev/assets/js/modules/app/main.js',
          'dev/assets/js/modules/app/controllers/login.js',
          'dev/assets/js/modules/app/controllers/home.js',
          'dev/assets/js/modules/app/services/auth.js',
          'dev/assets/js/modules/app/directives/message.js',
        ],
        dest: 'prod/assets/js/app.js'
      }
    },

    // minify js task
    uglify: {
      options: {
        // includes "use strict" statement
        banner: "/**" +
                "\n * <%= pkg.name %>" +
                "\n *" +
                "\n * <%= pkg.description %>" +
                "\n *" +
                "\n * @version v<%= pkg.version %>" +
                "\n * @build   <%= grunt.template.today('yyyy/mm/dd') %>" +
                "\n * @author  <%= pkg.author.name %>" +
                "\n * @email   <%= pkg.author.email %>" +
                "\n * @link    <%= pkg.homepage %>" +
                "\n */" +
                "\n\n'use strict';\n"
      },
      prod_js: {
        files: {
          'prod/assets/js/app.min.js': ['prod/assets/js/app.js']
        }
      }
    },

    // minify css task
    cssmin: {
      combine: {
        files: {
          'prod/assets/css/app.min.css': [
            'dev/assets/js/vendor/bootstrap/docs/assets/css/bootstrap.css',
            'dev/assets/css/app.css'
          ]
        }
      }
    },

    // clean prod folder taks
    clean: {
      prod: {
        src: ['prod/assets/js/app.js']
      }
    }
  });

  // load tasks
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-clean');

  // default task
  grunt.registerTask('default', ['copy', 'concat', 'uglify', 'cssmin', 'clean']);
};